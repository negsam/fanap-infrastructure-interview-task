FROM golang:1.18-alpine AS builder

WORKDIR /opt

ADD go.mod ./
ADD go.sum ./
RUN go mod download

ADD main.go ./
ADD models.go ./
RUN go build -o serverd main.go models.go


FROM alpine:latest AS server

WORKDIR /opt

COPY --from=builder /opt/serverd ./

EXPOSE 8070
CMD ["/opt/serverd"]
