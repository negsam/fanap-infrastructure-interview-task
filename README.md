## Running

```bash
docker compose up --build
```

## Testing
The server will be listening on port `8070`

You can also run the test script to send test cases specified in the task document to the server.
```bash
./test/test.sh
```
