package main

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/spf13/viper"

	"github.com/gin-gonic/gin"
)

func loadConfig() (*viper.Viper, error) {
	config := viper.New()

	config.SetConfigName("config")
	config.SetConfigType("yaml")
	config.AddConfigPath("./config/")

	if err := config.ReadInConfig(); err != nil {
		return nil, err
	}

	if err := config.BindEnv("DB_PASSWORD"); err != nil {
		return nil, err
	}

	return config, nil
}

func NewConnectionPool(config *viper.Viper) (*pgxpool.Pool, error) {
	host := config.GetString("db.host")
	port := config.GetUint16("db.port")
	username := config.GetString("db.user")
	password := config.GetString("DB_PASSWORD")
	maxConns := config.GetInt32("db.max_conns")

	connString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=appdb pool_max_conns=%d sslmode=disable",
		host, port, username, password, maxConns)

	cfg, err := pgxpool.ParseConfig(connString)
	if err != nil {
		return nil, err
	}

	return pgxpool.NewWithConfig(context.Background(), cfg)
}

type Handler struct {
	conn *pgxpool.Pool
}

func NewHandler(connPool *pgxpool.Pool) *Handler {
	return &Handler{
		conn: connPool,
	}
}

func (h *Handler) storeRectangles(c *gin.Context, recs []Rectangle) error {
	t := time.Now().Format("2006-01-02 15:04:05")

	valuesStr := make([]string, 0)
	rectangles := make([]interface{}, 0)
	for idx, rec := range recs {
		str := fmt.Sprintf("($%d, $%d, $%d, $%d, $%d)",
			5*idx+1,
			5*idx+2,
			5*idx+3,
			5*idx+4,
			5*idx+5,
		)
		valuesStr = append(valuesStr, str)
		rectangles = append(rectangles, rec.X, rec.Y, rec.Width, rec.Height, t)
	}

	query := "INSERT INTO rectangles (x, y, width, height, time) VALUES " + strings.Join(valuesStr, ", ")
	_, err := h.conn.Exec(c, query, rectangles...)
	return err
}

func (h *Handler) handleGet(c *gin.Context) {
	rows, err := h.conn.Query(c, "SELECT x, y, width, height, time FROM rectangles ")
	if err != nil {
		c.AbortWithError(500, err)
		return
	}

	rectangles, err := pgx.CollectRows(rows, rowToRectangle)
	if err != nil {
		c.AbortWithError(500, err)
		return
	}
	c.JSON(http.StatusOK, rectangles)

}

func (h *Handler) handlePost(c *gin.Context) {
	d := &Data{}
	if err := c.ShouldBindJSON(d); err != nil {
		c.AbortWithStatus(400)
		return
	}
	filteredRectangles := make([]Rectangle, 0)

	for _, v := range d.Input {
		if v.Intersects(&d.Main) {
			filteredRectangles = append(filteredRectangles, v)
		}
	}

	if err := h.storeRectangles(c, filteredRectangles); err != nil {
		c.AbortWithError(500, err)
		return
	}
	c.Status(201)
}

func main() {
	config, err := loadConfig()
	if err != nil {
		fmt.Println("Failed in loading config", err)
		return
	}

	connPool, err := NewConnectionPool(config)
	if err != nil {
		fmt.Println("Failed in creating connection pool", err)
		return
	}

	h := NewHandler(connPool)

	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		h.handleGet(c)
	})
	r.POST("/", func(c *gin.Context) {
		h.handlePost(c)
	})

	fmt.Println("Starting the server")

	listenPort := config.GetUint("server.port")
	r.Run(fmt.Sprintf(":%d", listenPort))

	fmt.Println("exiting")
}
