package main

import "github.com/jackc/pgx/v5"

type Rectangle struct {
	X      int     `json:"x"`
	Y      int     `json:"y"`
	Width  int     `json:"width"`
	Height int     `json:"height"`
	Time   *string `json:"time"`
}

func newRectangle() *Rectangle {
	return &Rectangle{
		Time: new(string),
	}
}

func rowToRectangle(row pgx.CollectableRow) (*Rectangle, error) {
	rec := newRectangle()
	if err := row.Scan(&rec.X, &rec.Y, &rec.Width, &rec.Height, rec.Time); err != nil {
		return nil, err
	}

	return rec, nil

}

func (r *Rectangle) Intersects(other *Rectangle) bool {
	x := r.X + r.Width
	y := r.Y + r.Height
	otherX := other.X + other.Width
	otherY := other.Y + other.Height
	if other.X < x && x < otherX && other.Y < y && y < otherY {
		return true
	}
	if other.X < x && x < otherX && other.Y < r.Y && r.Y < otherY {
		return true
	}
	if other.X < r.X && r.X < otherX && other.Y < y && y < otherY {
		return true
	}
	if other.X < r.X && r.X < otherX && other.Y < r.Y && r.Y < otherY {
		return true
	}

	return false
}

type Data struct {
	Main  Rectangle   `json:"main"`
	Input []Rectangle `json:"input"`
}
